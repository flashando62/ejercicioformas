﻿using System;
using System.Collections.Generic;
using WildarChallenge.Data.Classes;
using WildarChallenge.Data.Classes.Formas;
using NUnit.Framework;

namespace WildarChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        #region Formas
        public const int Cuadrado = 1;
        public const int TrianguloEquilatero = 2;
        public const int Circulo = 3;
        public const int Trapecio = 4;
        public const int Rectangulo = 5;
        #endregion
        #region Idiomas
        public const int Castellano = 1;
        public const int Ingles = 2;
        public const int Italiano = 3;
        #endregion

        [TestCase]
        public void TestResumenListaVacia()
        {
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>",
                FormaGeometrica.Imprimir(new List<FormaGeometrica>(), 1));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            Assert.AreEqual("<h1>Empty list of shapes!</h1>",
                FormaGeometrica.Imprimir(new List<FormaGeometrica>(), 2));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnItaliano()
        {
            Assert.AreEqual("<h1>Lista vuota di forme!</h1>",
                FormaGeometrica.Imprimir(new List<FormaGeometrica>(), 3));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            var cuadrados = new List<FormaGeometrica> {new Cuadrado(5)};

            var resumen = FormaGeometrica.Imprimir(cuadrados, Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perimetro 20 <br/>TOTAL:<br/>1 formas Perimetro 20 Area 25", resumen);
        }
        [TestCase]
        public void TestResumenListaConUnRectangulo()
        {
            var formas = new List<FormaGeometrica> { new Rectangulo(2, 1) };

            var resumen = FormaGeometrica.Imprimir(formas, Italiano);

            Assert.AreEqual("<h1>Rapporto sui moduli</h1>1 Rettangolo | La zona 2 | Perimetro 6 <br/>TOTALE:<br/>1 forme Perimetro 6 La zona 2", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            var cuadrados = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new Cuadrado(1),
                new Cuadrado(3)
            };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            var formas = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = FormaGeometrica.Imprimir(formas, Ingles);

            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>TOTAL:<br/>7 shapes Perimeter 97,66 Area 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var formas = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = FormaGeometrica.Imprimir(formas, Castellano);

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perimetro 28 <br/>2 Círculos | Area 13,01 | Perimetro 18,06 <br/>3 Triángulos | Area 49,64 | Perimetro 51,6 <br/>TOTAL:<br/>7 formas Perimetro 97,66 Area 91,65",
                resumen);
        }
        [TestCase]
        public void TestResumenListaConMasTiposEnItaliano()
        {
            var formas = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = FormaGeometrica.Imprimir(formas, Italiano);

            Assert.AreEqual(
                "<h1>Rapporto sui moduli</h1>2 Piazze | La zona 29 | Perimetro 28 <br/>2 Cerchi | La zona 13,01 | Perimetro 18,06 <br/>3 Triangoli | La zona 49,64 | Perimetro 51,6 <br/>TOTALE:<br/>7 forme Perimetro 97,66 La zona 91,65",
                resumen);
        }

        #region TestCalculoArea
        [TestCase]
        public void TestCaluloAreaCirculo()
        { 
            var Area = Math.Round(new Circulo(3).CalcularArea());
            Assert.AreEqual(7, Area);
        }

        [TestCase]
        public void TestCaluloAreaCuadrado()
        { 
            var Area = Math.Round(new Cuadrado(5).CalcularArea());
            Assert.AreEqual(25, Area);
        }

        [TestCase]
        public void TestCaluloAreaTrianguloEquilatero()
        {
            var Area = Math.Round(new TrianguloEquilatero(4).CalcularArea());
            Assert.AreEqual(7,Area);
        }

        [TestCase]
        public void TestCaluloAreaRectangulo()
        {
            var Area = Math.Round(new Rectangulo(2, 1).CalcularArea());
            Assert.AreEqual(2, Area);
        }

        [TestCase]
        public void TestCaluloAreaTrapecioIsosceles()
        {
            var Area = Math.Round(new TrapecioIsosceles(2, 1, 3).CalcularArea());
            Assert.AreEqual( 4, Area);
        }
        #endregion

        #region TestCalculoPerimetro
        [TestCase]
        public void TestCaluloPerimetroCirculo()
        {
            var Perimetro = Math.Round(new Circulo(3).CalcularPerimetro());
            Assert.AreEqual(9,Perimetro);
        }

        [TestCase]
        public void TestCaluloPerimetroCuadrado()
        {
            var Perimetro = Math.Round(new Cuadrado(5).CalcularPerimetro());
            Assert.AreEqual(20, Perimetro);
        }

        [TestCase]
        public void TestCaluloPerimetroTrianguloEquilatero()
        {
            var Perimetro = Math.Round(new TrianguloEquilatero(4).CalcularPerimetro());
            Assert.AreEqual(12,Perimetro);
        }

        [TestCase]
        public void TestCaluloPerimetroRectangulo()
        {
            var Perimetro = Math.Round(new Rectangulo(2, 1).CalcularPerimetro());
            Assert.AreEqual(6,Perimetro);
        }

        [TestCase]
        public void TestCaluloPerimetroTrapecioIsosceles()
        {
            var Perimetro = Math.Round(new TrapecioIsosceles(2, 1, 3).CalcularPerimetro());
            Assert.AreEqual(9,Perimetro);
        }
        #endregion
    }
}
