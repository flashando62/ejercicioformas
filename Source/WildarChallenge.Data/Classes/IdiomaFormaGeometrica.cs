﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildarChallenge.Data.Classes
{
    public class IdiomaFormaGeometrica
    {
        public int Key { get; set; }
        public string ListaVacia { get; set; }
        public string Reporte { get; set; }
        public string Total { get; set; }
        public string Perimetro { get; set; }
        public string Area { get; set; }
        public string Formas { get; set; }
        public List<Tuple<int,string,string>> FormasSingularYPlural { get; set; }


        public IdiomaFormaGeometrica(int key, string listaVacia,string reporte,string total,string perimetro,string area,string formas, List<Tuple<int,string,string>> formasSingularYPlural)
        {
            Key = key;
            ListaVacia = listaVacia;
            Reporte = reporte;
            Total = total;
            FormasSingularYPlural = formasSingularYPlural;
            Perimetro = perimetro;
            Area = area;
            Formas = formas;
        }

        public string TraducirForma(int tipo,int cantidad)
        {
            try
            {
                var coincidencia = FormasSingularYPlural.Find(x => x.Item1 == tipo);
                return cantidad > 1 ? coincidencia.Item3 : coincidencia.Item2;
            }catch(Exception e)
            {
                throw new Exception("No se ecnontro traduccion para esa forma");
            }

        }
    }
}
