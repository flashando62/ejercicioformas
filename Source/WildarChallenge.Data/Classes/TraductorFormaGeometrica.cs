﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildarChallenge.Data.Classes
{
    public class TraductorFormaGeometrica
    {
        private  List<IdiomaFormaGeometrica> Idiomas { get; set; }

        public TraductorFormaGeometrica()
        {
            Idiomas = new List<IdiomaFormaGeometrica>();
            var Castellano = new IdiomaFormaGeometrica(
                        1,
                        "Lista vacía de formas!",
                        "Reporte de Formas",
                        "Total",
                        "Perimetro",
                        "Area",
                        "Formas",
                        new List<Tuple<int, string, string>>
                        {
                            new Tuple<int, string, string>(1,"Cuadrado","Cuadrados"),
                            new Tuple<int, string, string>(2,"Triángulo", "Triángulos"),
                            new Tuple<int, string, string>(3,"Círculo", "Círculos"),
                            new Tuple<int, string, string>(4,"Trapecio", "Trapecios"),
                            new Tuple<int, string, string>(5,"Rectangulo", "Rectangulos")
                        }
                    );
                
            var Ingles = new IdiomaFormaGeometrica(
                    2, 
                    "Empty list of shapes!",
                    "Shapes report",
                    "Total",
                    "Perimeter",
                    "Area",
                    "Shapes",
                    new List<Tuple<int, string, string>>
                    {
                        new Tuple<int, string, string>(1,"Square","Squares"),
                        new Tuple<int, string, string>(2,"Triangle","Triangles"),
                        new Tuple<int, string, string>(3,"Circle","Circles"),
                        new Tuple<int, string, string>(4,"trapeze","trapezoids"),
                        new Tuple<int, string, string>(5,"Rectangle","Rectangles")
                    }
                );
            var Italiano = new IdiomaFormaGeometrica(
                        3,
                        "Lista vuota di forme!",
                        "Rapporto sui moduli",
                        "Totale",
                        "Perimetro",
                        "La zona",
                        "Forme",
                        new List<Tuple<int, string, string>>
                        {
                            new Tuple<int, string, string>(1,"Piazza","Piazze"),
                            new Tuple<int, string, string>(2,"Triangolo", "Triangoli"),
                            new Tuple<int, string, string>(3,"Cerchio", "Cerchi"),
                            new Tuple<int, string, string>(4,"Trapezio", "Trapezi"),
                            new Tuple<int, string, string>(5,"Rettangolo","Rettangoli")
                        }
                    );

            Idiomas.Add(Castellano);
            Idiomas.Add(Ingles);
            Idiomas.Add(Italiano);
        }

        public IdiomaFormaGeometrica DetectarIdioma(int keyIdioma)
        {
            try
            {
               return Idiomas.First(x => x.Key == keyIdioma);

            }catch(Exception e)
            {
                return Idiomas.First(x => x.Key == 2);//Default engish
            }
        }
    }
}
