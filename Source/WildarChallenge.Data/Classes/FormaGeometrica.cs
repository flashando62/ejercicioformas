﻿/******************************************************************************************************************/
/******* ¿Qué pasa si debemos soportar un nuevo idioma para los reportes, o agregar más formas geométricas? *******/
/******************************************************************************************************************/

/*
 * TODO: 
 * Refactorizar la clase para respetar principios de la programación orientada a objetos.
 * Implementar la forma Trapecio/Rectangulo. 
 * Agregar el idioma Italiano (o el deseado) al reporte.
 * Se agradece la inclusión de nuevos tests unitarios para validar el comportamiento de la nueva funcionalidad agregada (los tests deben pasar correctamente al entregar la solución, incluso los actuales.)
 * Una vez finalizado, hay que subir el código a un repo GIT y ofrecernos la URL para que podamos utilizar la nueva versión :).
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WildarChallenge.Data.Classes.Formas;

namespace WildarChallenge.Data.Classes
{
    public abstract class FormaGeometrica
    {
        
        public readonly decimal _lado;
        public int Tipo;
        public abstract decimal CalcularArea();
        public abstract decimal CalcularPerimetro();
        public FormaGeometrica(decimal ancho)
        {
            _lado = ancho;
        }
        public static string Imprimir(List<FormaGeometrica> formas, int idioma)
        {
            TraductorFormaGeometrica TraductorDeFormas = new TraductorFormaGeometrica();
            var sb = new StringBuilder();
            var Idioma = TraductorDeFormas.DetectarIdioma(idioma);
            if (!formas.Any())
            {
                sb.Append("<h1>" + Idioma.ListaVacia + "</h1>");
            }
            else
            {
                // Hay por lo menos una forma
                // HEADER
                    sb.Append("<h1>"+Idioma.Reporte+"</h1>");

                decimal AreaTotal = 0;
                decimal PerimetroTotal = 0;
                decimal CantidadTotal = 0;
                var tipos= formas.Select(x => x.Tipo).Distinct();
                foreach(int tipo in tipos)
                {
                    var elementos = formas.Where(x => x.Tipo == tipo).ToList();
                    var cantidad = elementos.Count;
                    decimal areaForma = 0;
                    decimal perimetroForma=0;
                    for (var i = 0; i < cantidad; i++)
                    {
                        var lado = elementos[i]._lado;
                        areaForma += elementos[i].CalcularArea();
                        perimetroForma += elementos[i].CalcularPerimetro();

                    }
                    sb.Append(ObtenerLinea(cantidad, areaForma, perimetroForma, tipo, Idioma));
                    AreaTotal += areaForma;
                    PerimetroTotal += perimetroForma;
                    CantidadTotal += cantidad;
                }

                // FOOTER
                sb.Append(Idioma.Total.ToUpper()+":<br/>");
                sb.Append(CantidadTotal + " " + Idioma.Formas.ToLower() + " ");
                sb.Append(Idioma.Perimetro+ " " + PerimetroTotal.ToString("#.##") + " ");
                sb.Append(Idioma.Area+" " + AreaTotal.ToString("#.##"));
            }

            return sb.ToString();
        }

        private static string ObtenerLinea(int cantidad, decimal area, decimal perimetro, int tipo, IdiomaFormaGeometrica idioma)
        {
            return $"{cantidad} {idioma.TraducirForma(tipo, cantidad)} | {idioma.Area} {area:#.##} | {idioma.Perimetro} {perimetro:#.##} <br/>";
        }

    }
}
