﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildarChallenge.Data.Classes.Formas
{
    public class TrapecioIsosceles : FormaGeometrica
    {
        private readonly decimal BaseSup;
        private readonly decimal Altura;
        public TrapecioIsosceles(decimal ancho, decimal baseSup, decimal altura) : base(ancho)
        {
            Tipo = 4;
            BaseSup = baseSup;
            Altura = altura;
        }

        public override decimal CalcularArea()
        {
            return ((_lado + BaseSup) / 2) * Altura;
        }

        public override decimal CalcularPerimetro()
        {
            return (decimal)(2 * Math.Sqrt(((double)((Altura * Altura) + ((BaseSup - _lado / 2) * (BaseSup - _lado / 2))))) + (double)BaseSup + (double)_lado);
        }
    }
}
