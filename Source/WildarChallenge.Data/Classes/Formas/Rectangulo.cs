﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildarChallenge.Data.Classes.Formas
{
    public class Rectangulo: FormaGeometrica
    {
        public decimal BaseForma { get; }
        public Rectangulo(decimal ancho, decimal baseForma) : base(ancho)
        {
            Tipo = 5;
            BaseForma = baseForma;
        }


        public override decimal CalcularArea()
        {
            return BaseForma * _lado;
        }

        public override decimal CalcularPerimetro()
        {
            return (BaseForma + _lado) * 2;
        }
    }
}
