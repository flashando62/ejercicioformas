﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildarChallenge.Data.Classes.Formas
{
    public class Cuadrado : FormaGeometrica
    {
        public Cuadrado(decimal ancho) : base( ancho)
        {
            Tipo = 1;
        }

        public override decimal CalcularArea()
        {
            return _lado * _lado;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado * 4;
        }
    }
}
