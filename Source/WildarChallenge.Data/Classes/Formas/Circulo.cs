﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildarChallenge.Data.Classes.Formas
{
    public class Circulo : FormaGeometrica
    {
        public Circulo(decimal ancho) : base(ancho)
        {
            Tipo = 3;
        }

        public override decimal CalcularArea()
        {
            return (decimal)Math.PI * (_lado / 2) * (_lado / 2);
        }

        public override decimal CalcularPerimetro()
        {
            return (decimal)Math.PI * _lado;
        }
    }
}
